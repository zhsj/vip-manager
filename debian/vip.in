# The following keys are mandatory

# The virtual IP address that will be managed
VIP_IP="@VIP_IP@"

# The netmask that is associated with the subnet that the virtual IP vip is
# part of.
VIP_MASK="@VIP_MASK@"
VIP_NETMASK="@VIP_MASK@"

# A local network interface on the machine that runs vip-manager. Required when
# using manager-type=basic. The vip will be added to and removed from this
# interface.
VIP_IFACE="@VIP_IFACE@"

# The key in the DCS that will be monitored by vip-manager. Must match
# <namespace>/<scope>/leader from Patroni config. When the value returned by
# the DCS equals trigger-value, vip-manager will make sure that the virtual IP
# is registered to this machine. If it does not match, vip-manager makes sure
# that the virtual IP is not registered to this machine.
VIP_KEY="@VIP_KEY@"
VIP_TRIGGER_KEY="@VIP_KEY@"

# The following keys are optional

# The value that the DCS' answer for trigger-key will be matched to. Must match
# <name> from Patroni config. This is usually set to the name of the patroni
# cluster member that this vip-manager instance is associated with. Defaults to
# the machine's hostname.
VIP_HOST="@VIP_HOST@"
VIP_TRIGGER_VALUE="@VIP_HOST@"

# Either basic or hetzner. This describes the mechanism that is used to manage
# the virtual IP. Defaults to basic.
#VIP_MANAGER_TYPE=basic

# The type of DCS that vip-manager will use to monitor the trigger-key.
# Defaults to etcd.
VIP_TYPE="@VIP_TYPE@"

# A url that defines where to reach the DCS. Multiple endpoints can be passed
# to the flag or env variable using a comma-separated-list. In the config file,
# a list can be specified, see the sample config for an example. Defaults to
# http://127.0.0.1:2379 for dcs-type=etcd and http://127.0.0.1:8500 for
# dcs-type=consul.
VIP_DCS_ENDPOINTS="@VIP_ENDPOINT@"
VIP_ENDPOINT="@VIP_ENDPOINT@"

# A username that is allowed to look at the trigger-key in an etcd DCS.
# Optional when using dcs-type=etcd .
#VIP_ETCD_USER=patroni 	

# The password for etcd-user. Optional when using dcs-type=etcd. Requires that
# etcd-user is also set.
#VIP_ETCD_PASSWORD=

# A token that can be used with the consul-API for authentication. Optional
# when using dcs-type=consul.
#VIP_CONSUL_TOKEN=

# The time vip-manager main loop sleeps before checking for changes. Measured
# in ms. Defaults to 1000.
#VIP_INTERVAL=1000

# The time to wait before retrying interactions with components outside of
# vip-manager. Measured in ms. Defaults to 250.
#VIP_RETRY_AFTER=250

# The number of times interactions with components outside of vip-manager are
# retried. Defaults to 3.
#VIP_RETRY_NUM=3

# A certificate authority file that can be used to verify the certificate
# provided by etcd endpoints. Make sure to change dcs-endpoints to reflect that
# https is used.
#VIP_ETCD_CA_FILE=/etc/etcd/ca.cert.pem

# A client certificate that is used to authenticate against etcd endpoints.
# Requires etcd-ca-file to be set as well.
#VIP_ETCD_CERT_FILE=/etc/etcd/client.cert.pem

# A private key for the client certificate, used to decrypt messages sent by
# etcd endpoints. Required when etcd-cert-file is specified.
#VIP_ETCD_KEY_FILE=/etc/etcd/client.key.pem

# Enable more verbose logging. Currently only the manager-type=hetzner provides
# additional logs.
#VIP_VERBOSE=false
